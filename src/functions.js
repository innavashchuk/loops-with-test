function flipSwitch(arr) {
    let result = [];
    let state = true
    for (let i = 0; i < arr.length; i++) {
        if (arr[i] === 'flip') state = !state;
        result.push(state)
    }
    return result;
}

module.exports = {
    flipSwitch
};
